Submission README
=========
---

Personal Details (Students Fill In Below)
===
---
- **Student Name**: Arvids Gargurnis
- **Student Number**: 10535807

- **Course**: Computing & Games Development
- **Module**: AINT151
- **Assignment Title**: Developer Website

3. **Credits**
* Starter Kit - https://github.com/DevTips/DevTips-Starter-Kit
* Chart JS - http://www.chartjs.org/
* Magnific Popup - http://dimsemenov.com/plugins/magnific-popup/
* Typicons Font Icons - http://www.typicons.com/
* Spinner Loader - http://projects.lukehaas.me/css-loaders/
* Prepros Sass Compiler - https://prepros.io/
* Phaser - https://phaser.io/
* Bounce JS Animations - http://bouncejs.com/
* Diner artwork - https://s-media-cache-ak0.pinimg.com/736x/0e/29/0b/0e290be6208ac2f60de92f759d5d1849.jpg
* Girl artwork - http://www.vecteezy.com/vector-art/84214-free-50-s-drive-in-diner-vector-ilustration
* Diner foods - http://vectorgraphicsblog.com/wp-content/uploads/2011/05/fast-food-vector-set.jpg
* On the fly logo - http://ontheflycomics.com/images/on_the_fly_logo_home.gif
* Top down characters - http://tdeleeuw.deviantart.com/art/Top-down-Characters-pixel-art-424298098
* Dripping blood - http://i.istockimg.com/file_thumbview_approve/17682502/6/stock-illustration-17682502-dripping-blood-seamless-.jpg
* Graphic text - http://cooltext.com/

4. **Comments**
* When validating the css file the errors within the main.css are from other sources. The reason why it is in the main.css is because I am using sass and when compiling different sass and other files they all get compiled in a single css file.

---
