$(document).ready(function() {
  smoothScroll(300);
  workShowcase();
  workLoad();
  blogShowcase();
  blogLoad();
  blogStuff();
});

function workLoad() {
  $.ajaxSetup({cache: true});
  $('.work-window').click(function() {
    var $this = $(this),
      newName = $this.data('name'),
			spinner = '<div class="loader">Loading...</div>';
      newHTML = '/work/'+ newName +'.html';
    $('.project-load').html(spinner).load(newHTML);
  })
}

function workShowcase() {
  $('.work-window').click(function() {
    $('.work-slider').css('right', '100%');
    $('.work-showcase').show();
  });
  $('.back-button').click(function() {
    $('.work-slider').css('right', '0%');
    $('.work-showcase').hide(800);
  })
}

function blogLoad() {
  $.ajaxSetup({cache: true});
  $('.blog-window').click(function() {
    var $this = $(this),
      newName = $this.data('name'),
			spinner = '<div class="loader">Loading...</div>';
      newHTML = '/blog/'+ newName +'.html';
    $('.project-load-blog').html(spinner).load(newHTML);
  })
}

function blogShowcase() {
  $('.blog-window').click(function() {
    $('.blog-slider').css('right', '0%');
    $('.full-blog').show();
  });
  $('.back-button').click(function() {
    $('.blog-slider').css('right', '100%');
    $('.full-blog').hide(800);
  })
}

function blogStuff() {
  $('.blog-line').first().addClass('active-blog')

  $('.blog-next, .blog-prev').click(function() {
    var $this = $(this),
      curActiveblog = $('.blog-line-changer').find('.active-blog'),
      position = $('.blog-line-changer').children().index(curActiveblog),
      blogNum = $('.blog-line').length;
    if($this.hasClass('blog-next')) {
      if(position < blogNum -1){
        $('.active-blog').removeClass('active-blog').next().addClass('active-blog');
      } else {
        $('.blog-line').removeClass('active-blog').first().addClass('active-blog');
    }
  } else {
    if (position === 0) {
      $('.blog-line').removeClass('active-blog').last().addClass('active-blog');
    } else {
    $('.active-blog').removeClass('active-blog').prev().addClass('active-blog');
      }
    }
  });
}

function smoothScroll (duration) {
	$('a[href^="#"]').on('click', function(event) {
	    var target = $( $(this).attr('href') );
	    if( target.length ) {
	        event.preventDefault();
	        $('html, body').animate({
	            scrollTop: target.offset().top
	        }, duration);
	    }
	});
}

var chartsDrawed = false;
$(window).scroll(function() {
	var wScroll = $(this).scrollTop();
	if (chartsDrawed == false && wScroll >= $('.about-section').offset().top) {
		drawCharts();
		chartsDrawed = true;
	}
})

function drawCharts() {

	var ctx = $("#pie-chart-commun").get(0).getContext("2d");

	var data = [
    {
        value: 200,
        color:"#A6A6A6"
    },
    {
        value: 160,
        color: "#E5E5E5"
    },
	]
	var myChart = new Chart(ctx).Doughnut(data, {
		segmentShowStroke: false,
});

var ctx = $("#pie-chart-organ").get(0).getContext("2d");

var data = [
	{
			value: 250,
			color:"#717171"
	},
	{
			value: 110,
			color: "#E5E5E5"
	},
]
var myChart = new Chart(ctx).Doughnut(data, {
	segmentShowStroke: false
});

var ctx = $("#pie-chart-learn").get(0).getContext("2d");

var data = [
	{
			value: 220,
			color:"#515151"
	},
	{
			value: 140,
			color: "#E5E5E5"
	},
]
var myChart = new Chart(ctx).Doughnut(data, {
	segmentShowStroke: false
});

var ctx = $("#pie-chart-teach").get(0).getContext("2d");

var data = [
	{
			value: 160,
			color:"#313131"
	},
	{
			value: 200,
			color: "#E5E5E5"
	},
]
var myChart = new Chart(ctx).Doughnut(data, {
	segmentShowStroke: false
});

var ctx = $("#pie-chart-visual").get(0).getContext("2d");

var data = [
	{
			value: 240,
			color:"#A6A6A6"
	},
	{
			value: 120,
			color: "#E5E5E5"
	},
]
var myChart = new Chart(ctx).Doughnut(data, {
	segmentShowStroke: false
});

var ctx = $("#pie-chart-ux").get(0).getContext("2d");

var data = [
	{
			value: 280,
			color:"#717171"
	},
	{
			value: 80,
			color: "#E5E5E5"
	},
]
var myChart = new Chart(ctx).Doughnut(data, {
	segmentShowStroke: false
});

var ctx = $("#pie-chart-design").get(0).getContext("2d");

var data = [
	{
			value: 320,
			color:"#515151"
	},
	{
			value: 40,
			color: "#E5E5E5"
	},
]
var myChart = new Chart(ctx).Doughnut(data, {
	segmentShowStroke: false
});

var ctx = $("#pie-chart-prog").get(0).getContext("2d");

var data = [
	{
			value: 250,
			color:"#313131"
	},
	{
			value: 110,
			color: "#E5E5E5"
	},
]
var myChart = new Chart(ctx).Doughnut(data, {
	segmentShowStroke: false
});

};
