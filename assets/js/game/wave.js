var waveState = {

  create:function(){

    background = game.add.sprite(0,0,'ground');
    waveground = game.add.sprite(game.world.centerX, game.world.centerY, 'wavend');
    waveground.anchor.setTo(0.5);
    killText = game.add.text(420, 195, '', {font: '25px arial', fill: '#f00'});
    bonusText = game.add.text(420, 240, '', {font: '25px arial', fill: '#f00'});
    pointText = game.add.text(420, 280, '', {font: '25px arial', fill: '#f00'});
    timeText = game.add.text(500, 331, '', {font: '25px arial', fill: '#f00'});
    game.time.events.loop(1000, updateTime, this);
    bonusScore = playerHealth * 100;

  },

  update:function(){
    timeText.text = countdown;
    pointText.text = endPoints;
    bonusText.text = bonusPoints;
    killText.text = killPoints;

    if (endPoints < score) {
      updatePoints();
    }

    if (bonusPoints < bonusScore) {
      updateBonus();
    }

    if (killPoints < totalKill) {
      updateKills();
    }

    if (countdown === 0) {
      game.state.start(level);
    }
  }
}

function updatePoints() {
  endPoints += 10;
}

function updateKills() {
  killPoints++;
}

function updateBonus() {
  bonusPoints += 10;
}

function updateTime() {
  countdown--;
}
