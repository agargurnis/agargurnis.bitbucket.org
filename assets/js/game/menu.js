
var menuState = {
  create:function(){

    var menu = game.add.sprite(0,0,'menu');

    startText = game.add.text(200, 130, 'To Start Game Click On Ash', { font: '36px arial', fill: '#000' });
    startText.alpha = 0.1;
    game.add.tween(startText).to( { alpha: 1 }, 1000, Phaser.Easing.Linear.None, true, 0, 1000, true);

    button = game.add.sprite(game.world.centerX - 50, game.world.centerY, null);
    button.width = 80;
    button.height = 100;
    game.physics.arcade.enable(button);
    button.inputEnabled = true;
    button.input.useHandCursor = true;
    button.events.onInputDown.addOnce(this.start, this);

  },

  start: function () {

      game.state.start('Level1');

  }


}
