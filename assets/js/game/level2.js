
var level2 = {
  create:function(){
    countdown = 6;
    spawn = 1;
    killed = 0;
    bonusPoints = 0;
    game.physics.startSystem(Phaser.Physics.ARCADE);
    cursors = game.input.keyboard.createCursorKeys();

    background = game.add.sprite(0,0,'ground');
    player = game.add.sprite(game.world.centerX, game.world.centerY, 'player');

    player.anchor.setTo(0.5);
    game.physics.arcade.enable(player);

    enemies = game.add.group();
  	enemies.enableBody = true;
    playerHealthText = game.add.text(620, 16, '', { font: '32px arial', fill: '#fff' });

    level = "Level3";

    bullets = game.add.group();
  	bullets.enableBody = true;
    bullets.physicsBodyType = Phaser.Physics.ARCADE;
    bullets.createMultiple(50, 'bullet');
    bullets.setAll('checkWorldBounds', true);
    bullets.setAll('outOfBoundsKill', true);

    player.body.allowRotation = true;
    player.body.collideWorldBounds = true;
    player.bringToTop();

  },

  update:function(){
    player.rotation = game.physics.arcade.angleToPointer(player);

    if (spawn < 10) {
      while(enemies.countLiving() < spawn)
      {
        createEnemy2();
      }
    } else if (spawn === 10){
        createBoss2();
    }

    if (enemies.countLiving() === 0) {
      game.state.start("Wave");
    }

    if (game.input.activePointer.isDown)
    {
        fire();
    }

    game.physics.arcade.overlap(bullets, enemies, bulletHitEnemy, null, this);
    game.physics.arcade.overlap(player, enemies, enemyHitPlayer, null, this);

    moveEnemies();
    movePlayer();
  }
}

function fire() {
   if (game.time.now > nextFire && bullets.countDead() > 0)
    {
        nextFire = game.time.now + fireRate;

        bullet = bullets.getFirstDead();

        bullet.reset(player.x - 8, player.y - 8);

        game.physics.arcade.moveToPointer(bullet, 300);
    }
 }

 function bulletHitEnemy (bullet, enemy) {
     bullet.kill();
     enemy.kill();
     killed++;
     totalKill++;
     score += 3;

     if (killed % 5 === 0) {
       spawn++;
     }
 }

 function enemyHitPlayer (player, enemy) {
    playerHealth -= 1;
    enemy.kill();
    playerHealthText.text = 'Health: ' + playerHealth;

    if (playerHealth === 0){
      game.state.start("Over");
    }
 }

  function movePlayer() {
    if (cursors.left.isDown)
    {
        player.body.velocity.x = -200;
    }
    else if (cursors.right.isDown)
    {
        player.body.velocity.x = 200;
    }
    else
    {
      player.body.velocity.x = 0;
    }
    if (cursors.up.isDown)
    {
        player.body.velocity.y = -200;
    }
    else if (cursors.down.isDown)
    {
        player.body.velocity.y = 200;
    }
    else
    {
      player.body.velocity.y = 0;
    }
  }

  function moveEnemies() {
    enemies.forEach(function(enemy) {

      enemy.rotation = game.physics.arcade.angleBetween(enemy, player);
      enemy.anchor.setTo(0.5);

      if (player.body.x < enemy.body.x)
      {
        enemy.body.velocity.x = 70 * -1;
      }
      else
      {
        enemy.body.velocity.x = 70;
      }
        if (player.body.y < enemy.body.y)
      {
        enemy.body.velocity.y = 70 * -1;
      }
      else
      {
        enemy.body.velocity.y = 70;
      }
    }, this);
  }

  function createBoss2() {
    var randomSpawn = game.rnd.integerInRange(0, 5);
    var randomX = game.rnd.integerInRange(0, 778);
    var randomY = game.rnd.integerInRange(0, 576);
    switch(randomSpawn){
        case 0:
        enemy = enemies.create(randomX, -10, 'boss2');
        break;
        case 1:
        enemy = enemies.create(-10, randomY, 'boss2');
        break;
        case 2:
        enemy = enemies.create(randomX, 586, 'boss2');
        break;
        case 3:
        enemy = enemies.create(788, randomY, 'boss2');
        break;
        default:
    }
  }

  function createEnemy2(){
     var randomSpawn = game.rnd.integerInRange(0, 9);
     var randomX = game.rnd.integerInRange(0, 778);
     var randomY = game.rnd.integerInRange(0, 576);
     switch(randomSpawn){
         case 0:
         enemy = enemies.create(randomX, -10, 'enemy3');
         break;
         case 1:
         enemy = enemies.create(-10, randomY, 'enemy3');
         break;
         case 2:
         enemy = enemies.create(randomX, 586, 'enemy3');
         break;
         case 3:
         enemy = enemies.create(788, randomY, 'enemy3');
         break;
         case 4:
         enemy = enemies.create(randomX, -10, 'enemy4');
         break;
         case 5:
         enemy = enemies.create(-10, randomY, 'enemy4');
         break;
         case 6:
         enemy = enemies.create(randomX, 586, 'enemy4');
         break;
         case 7:
         enemy = enemies.create(788, randomY, 'enemy4');
         break;
         default:
     }
 }
