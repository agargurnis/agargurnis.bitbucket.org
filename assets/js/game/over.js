var overState = {

  create:function(){
    endPoints = 0;
    killPoints = 0;

    background = game.add.sprite(0,0,'ground');
    endground = game.add.sprite(game.world.centerX, game.world.centerY, 'end');
    endground.anchor.setTo(0.5);

    killText = game.add.text(420, 211, '', {font: '25px arial', fill: '#f00'});
    pointText = game.add.text(420, 245, '', {font: '25px arial', fill: '#f00'});

    var start = game.add.sprite(250, 280, null);
    start.width = 270;
    start.height = 40;
    game.physics.arcade.enable(start);
    start.inputEnabled = true;
    start.input.useHandCursor = true;
    start.events.onInputDown.addOnce(this.start, this);

    var menu = game.add.sprite(300, 330, null);
    menu.width = 175;
    menu.height = 50;
    game.physics.arcade.enable(menu);
    menu.inputEnabled = true;
    menu.input.useHandCursor = true;
    menu.events.onInputDown.addOnce(this.menu, this);

  },

  update:function() {
    pointText.text = endPoints;
    killText.text = killPoints;

    if (endPoints < score) {
      updatePoints();
    }

    if (killPoints < totalKill) {
      updateKills();
    }

  },

  start: function () {

      game.state.start('Level1');
  },

  menu: function () {

      game.state.start('Menu');
  }
}

function updatePoints() {
  endPoints += 10;
}

function updateKills() {
  killPoints++;
}
