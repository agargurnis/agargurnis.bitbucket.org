
var preloadState = {
  preload:function(){
		var loadingBar = this.add.sprite(game.world.centerX, game.world.centerY,'loading');
		loadingBar.anchor.setTo(0.5);
		game.load.setPreloadSprite(loadingBar);
		game.load.image('ground', 'assets/ash/tabletop.png');
		game.load.image('menu', 'assets/ash/menu.png');
		game.load.image('player', 'assets/ash/player.png');
		game.load.image('bullet', 'assets/ash/bullet.png');
		game.load.image('enemy1', 'assets/ash/enemy1.png');
		game.load.image('enemy2', 'assets/ash/enemy2.png');
		game.load.image('enemy3', 'assets/ash/enemy3.png');
		game.load.image('enemy4', 'assets/ash/enemy4.png');
    game.load.image('enemy5', 'assets/ash/enemy5.png');
    game.load.image('enemy6', 'assets/ash/enemy6.png');
		game.load.image('boss1', 'assets/ash/boss3.png');
		game.load.image('boss2', 'assets/ash/boss2.png');
    game.load.image('boss3', 'assets/ash/boss1.png');
    game.load.image('wavend', 'assets/ash/wavend.png');
    game.load.image('end', 'assets/ash/gameover.png');
    game.load.image('splat', 'assets/ash/splat.png');
  },

  create:function(){
		game.state.start("Menu")

  }

}
