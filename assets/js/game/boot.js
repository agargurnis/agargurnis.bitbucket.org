var game = new Phaser.Game(778, 576, Phaser.AUTO, 'gameDiv');
var player;
var background;
var waveground;
var enemies;
var enemy;
var bullets;
var bullet;
var fireRate = 100;
var nextFire = 0;
var spawn = 1;
var enemiesKilled = 0;
var playerHealth = 5;
var score = 0;
var killed = 0;
var totalKill = 0;
var scoreText;
var playerHealthText;
var countdown = 6;
var endPoints = 0;
var bonusPoints = 0;
var killPoints = 0;
var bonusScore;
var timeText;
var level;
var button;
var splat;

var bootState = {
  preload:function(){
    game.load.image('loading', 'assets/ash/loading.png');
  },

  create:function(){

		this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		//this.scale.pageAlignHorizontally = true;
		//this.scale.setScreenSize();
		this.game.state.start("Preload");

  }

}
