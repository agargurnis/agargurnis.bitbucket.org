$(document).ready(function() {
	$('.image-link').magnificPopup({type:'image'});
  enLarge();
});

function enLarge() {
  $('.parent-container').magnificPopup({
    delegate: 'div',
    type: 'image',
		closeOnContentClick: true,
		zoom: {
			enabled: true,
			duration: 300
		}
  });
}
