$(document).ready(function() {
  workShowcase();
  workLoad();
});

function workLoad() {
  $.ajaxSetup({cache: true});
  $('.work-window').click(function() {
    var $this = $(this),
      newName = $this.data('name'),
			spinner = '<div class="loader">Loading...</div>';
      newHTML = '/work/'+ newName +'.html';
    $('.project-load').html(spinner).load(newHTML);
  })
}

function workShowcase() {
  $('.work-window').click(function() {
    $('.work-slider').css('left', '-100%');
    $('.work-showcase').show();
  });
  $('.back-button').click(function() {
    $('.work-slider').css('left', '0%');
    $('.work-showcase').hide(800);
  })
}
