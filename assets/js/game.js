var game = new Phaser.Game(800, 526, Phaser.AUTO, 'gameDiv');

var background;
var player;
var burgers;
var dogs;
var fries;
var drinks;
var cursors;
var scoreText;
var points = 0;
var lives;
var floor;
var alive;

var menuState = {
  preload:function(){
    game.load.image('menu', 'assets/diner/startscreen.png');
  },

  create:function(){

    var background = game.add.sprite(0,0,'menu');

    var start = game.add.sprite(580, 410, null);
    start.width = 100;
    start.height = 100;
    game.physics.arcade.enable(start);
    start.inputEnabled = true;
    start.input.useHandCursor = true;
    start.events.onInputDown.addOnce(this.start, this);

  },

  start: function () {

      game.state.start('playState');

  }


}

var overState = {
  preload:function(){
    game.load.image('diner', 'assets/diner/diner.png');
    game.load.image('ending', 'assets/diner/endscreen.png');
  },

  create:function(){

    var background = game.add.sprite(0,0,'diner');
    var result = game.add.sprite(250, 100, 'ending');
    var resultText = game.add.text(380, 200, '', {font: '36px Arial', fill: '#FFF'})
    resultText.text = points;

    var start = game.add.sprite(300, 260, null);
    start.width = 205;
    start.height = 50;
    game.physics.arcade.enable(start);
    start.inputEnabled = true;
    start.input.useHandCursor = true;
    start.events.onInputDown.addOnce(this.start, this);

    var menu = game.add.sprite(300, 320, null);
    menu.width = 205;
    menu.height = 50;
    game.physics.arcade.enable(menu);
    menu.inputEnabled = true;
    menu.input.useHandCursor = true;
    menu.events.onInputDown.addOnce(this.menu, this);

  },

  start: function () {

      game.state.start('playState');
  },

  menu: function () {

      game.state.start('menuState');
  }
}

var playState = {
  preload:function(){
    game.load.image('diner', 'assets/diner/diners.png');
    game.load.image('player', 'assets/diner/girl.png');
    game.load.image('burger', 'assets/diner/burger.png');
    game.load.image('dog', 'assets/diner/hotdog.png');
    game.load.image('fries', 'assets/diner/fries.png');
    game.load.image('drink', 'assets/diner/drink.png');
  },
  create:function(){

    game.physics.startSystem(Phaser.Physics.ARCADE);
    cursors = game.input.keyboard.createCursorKeys();

    background = game.add.sprite(0,0,'diner');
    player = game.add.sprite(400, 390, 'player');

    player.anchor.setTo(0.5);
    game.physics.arcade.enable(player);
    player.body.collideWorldBounds = true;
    player.body.setSize(160, 30, 0, -65);
    scoreText = game.add.text(630, 65, '00000', {font: '36px Arial', fill: '#FFF'})

    burgers = game.add.group();
  	burgers.enableBody = true;
  	dogs = game.add.group();
  	dogs.enableBody = true;
    fries = game.add.group();
  	fries.enableBody = true;
  	drinks = game.add.group();
  	drinks.enableBody = true;
    lives = game.add.group();

    game.time.events.loop(2500, createDog, this);
    game.time.events.loop(3700, createBurger, this);
    game.time.events.loop(6500, createFries, this);
    game.time.events.loop(7000, createDrink, this);

    life1 = lives.create(70, 60, 'drink');
    life2 = lives.create(110, 60, 'drink');
    life3 = lives.create(150, 60, 'drink');
    life4 = lives.create(190, 60, 'drink');

    floor = game.add.sprite(0, 520, null);
    game.physics.arcade.enable(floor);
    floor.body.setSize(800, 20, 0, 0);

  },
  update:function(){

    alive = lives.countLiving();

    game.physics.arcade.overlap(player, burgers, catchBurger, null, this);
    game.physics.arcade.overlap(player, dogs, catchDogs, null, this);
    game.physics.arcade.overlap(player, fries, catchFries, null, this);
    game.physics.arcade.overlap(player, drinks, catchDrink, null, this);
    game.physics.arcade.overlap(floor, burgers, killBurger, killLife, this);
    game.physics.arcade.overlap(floor, dogs, killDogs, killLife, this);
    game.physics.arcade.overlap(floor, fries, killFries, killLife, this);
    game.physics.arcade.overlap(floor, drinks, killDrink, killLife, this);

    if (cursors.left.isDown)
    {
        player.body.velocity.x = -400;
        player.scale.setTo(1, 1);
    }
    else if (cursors.right.isDown)
    {
        player.body.velocity.x = 400;
        player.scale.setTo(-1, 1);
    }
    else
    {
      player.body.velocity.x = 0;
    }

    if (cursors.down.isDown && cursors.right.isDown) {
      player.body.velocity.x = 600;
      player.scale.setTo(-1, 1);

    }
    else if (cursors.down.isDown && cursors.left.isDown) {
      player.body.velocity.x = -600;
      player.scale.setTo(1, 1);
    }

		scoreText.text = points;

    if (alive === 0) {
      game.state.start('overState');
    }
  }

}

  function makeItRain() {
    var randomValue = game.rnd.integerInRange(0, 750);
  	burger = burgers.create(randomValue, 1, 'burger');
  	burger.body.gravity.y = 200;
  	hotdog = dogs.create(randomValue, 0, 'dog');
  	hotdog.body.gravity.y = 300;
  	drink = drinks.create(randomValue, 0, 'drink');
  	drink.body.gravity.y = 400;
  	frie = fries.create(randomValue, 0, 'fries');
  	frie.body.gravity.y = 100;
  }

  function catchBurger() {
    burger.kill();
    points += 3;
  }
  function catchFries() {
    frie.kill();
    points += 1;
  }
  function catchDogs() {
    hotdog.kill();
    points += 2;
  }
  function catchDrink() {
    drink.kill();
    points += 10;
    addLife();
  }

  function killBurger() {
    burger.kill();
  }
  function killFries() {
    frie.kill();
  }
  function killDogs() {
    hotdog.kill();
  }
  function killDrink() {
    drink.kill();
  }

  function killLife() {

    if (alive === 4) {
      life4.kill();
    } else if (alive === 3) {
      life3.kill();
    } else if (alive === 2) {
      life2.kill();
    } else if (alive === 1) {
      life1.kill();
    }
  }

  function addLife() {

    if (alive === 3) {
      life4 = lives.create(190, 60, 'drink');
    } else if (alive === 2) {
      life3 = lives.create(150, 60, 'drink');
    } else if (alive === 1) {
      life2 = lives.create(110, 60, 'drink');
    }
  }

  function createBurger() {
    var randomValue = game.rnd.integerInRange(0, 750);
  	burger = burgers.create(randomValue, 1, 'burger');
  	burger.body.gravity.y = 200;

  }
  function createDog() {
    var randomValue = game.rnd.integerInRange(0, 750);
  	hotdog = dogs.create(randomValue, 0, 'dog');
  	hotdog.body.gravity.y = 200;

  }
  function createDrink() {
    var randomValue = game.rnd.integerInRange(0, 750);
  	drink = drinks.create(randomValue, 0, 'drink');
  	drink.body.gravity.y = 200;

  }
  function createFries() {
    var randomValue = game.rnd.integerInRange(0, 750);
  	frie = fries.create(randomValue, 0, 'fries');
  	frie.body.gravity.y = 200;

  }

game.state.add('playState', playState);
game.state.add('menuState', menuState);
game.state.add('overState', overState);
game.state.start('menuState');
